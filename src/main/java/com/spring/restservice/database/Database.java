package com.spring.restservice.database;

import com.spring.restservice.model.Employee;
import com.spring.restservice.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Database implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(Database.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public void run(String... args) throws Exception {
        //Init Customer Database
        employeeRepository.save
                (new Employee("A", "Nguyen Van",
                        "033816255", "Ho Chi Minh",
                        "{\"name\": 'Back Office',\"description\": ''}"));
        employeeRepository.save
                (new Employee("Minh", "Tran Ha",
                        "086869575", "Hai Phong",
                        "{\"name\": 'Develop',\"description\": ''}"));
        employeeRepository.save
                (new Employee("Hai", "Nguyen Quang",
                        "033816255", "Ha Noi",
                        "{\"name\": 'Develop',\"description\": ''}"));
        employeeRepository.save
                (new Employee("Thanh", "Tran Van",
                        "033816255", "Da Nang",
                        "{\"name\": 'Tester',\"description\": ''}"));
    }
}
