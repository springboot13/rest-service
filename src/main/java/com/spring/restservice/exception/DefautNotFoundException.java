package com.spring.restservice.exception;

public class DefautNotFoundException extends RuntimeException {

    public DefautNotFoundException(Long id) {
        super("Data id not found : " + id);
    }

}
