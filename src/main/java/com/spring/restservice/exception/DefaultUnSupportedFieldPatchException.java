package com.spring.restservice.exception;

import java.util.Set;

public class DefaultUnSupportedFieldPatchException extends RuntimeException{

    public DefaultUnSupportedFieldPatchException(Set<String> keys) {
        super("The Fields " + keys.toString() + " update is not allow.");
    }
}
