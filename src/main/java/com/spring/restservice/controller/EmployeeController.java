package com.spring.restservice.controller;

import com.spring.restservice.model.Employee;
import com.spring.restservice.exception.DefaultUnSupportedFieldPatchException;
import com.spring.restservice.exception.DefautNotFoundException;
import com.spring.restservice.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/employee")
    public List<Employee> findEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employee/{id}")
    public Employee findOne(@PathVariable Long id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new DefautNotFoundException(id));
    }

    @PostMapping("/employee")
    @ResponseStatus(HttpStatus.OK)
    public Employee newEmployee(@RequestBody Employee newEmployee) {
        return employeeRepository.save(newEmployee);
    }

    @PutMapping("/employee/{id}")
    public Employee saveOrUpdate(@RequestBody Employee newEmployee, @PathVariable Long id) {
        return employeeRepository.findById(id)
                .map(c -> {
                    c.setFistName(newEmployee.getFistName());
                    c.setLastName(newEmployee.getLastName());
                    c.setAddress(newEmployee.getAddress());
                    c.setPhoneNumber(newEmployee.getPhoneNumber());
                    c.setDepartment(newEmployee.getDepartment());
                    return employeeRepository.save(c);
                })
                .orElseGet(() -> {
                    newEmployee.setId(id);
                    return employeeRepository.save(newEmployee);
                });
    }

    @DeleteMapping("/employee/{id}")
    public void deleteEmployee(@PathVariable Long id) {
       Optional<Employee> employee = employeeRepository.findById(id);
       if (employee.isPresent()) {
           employeeRepository.deleteById(id);
       } else {
           throw new DefautNotFoundException(id);
       }
    }

    @PatchMapping("/employee/{id}")
    public Employee patch(@RequestBody Map<String, String> update, @PathVariable Long id) {
        return employeeRepository.findById(id)
                .map(c -> {
                    String address = update.get("address");
                    if (Optional.ofNullable(address).isPresent()) {
                        c.setAddress(address);

                        return employeeRepository.save(c);
                    } else {
                        throw new DefaultUnSupportedFieldPatchException(update.keySet());
                    }
                })
                .orElseGet(() -> {
                    throw new DefautNotFoundException(id);
                });
    }

}
