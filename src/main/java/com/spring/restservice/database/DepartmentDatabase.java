package com.spring.restservice.database;

import com.spring.restservice.model.Department;
import com.spring.restservice.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

public class DepartmentDatabase implements CommandLineRunner {

    @Autowired
    private DepartmentRepository departmentRepository;


    @Override
    public void run(String... args) throws Exception {
        //Init Department database
        departmentRepository.save(new Department("Back Office", ""));
        departmentRepository.save(new Department("Develop", ""));
        departmentRepository.save(new Department("BA", ""));
        departmentRepository.save(new Department("Tester", ""));
    }
}
