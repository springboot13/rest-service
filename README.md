# Spring Boot Rest API

## 1. How to start project
```
$ cd rest-service
$ Run: mvn spring-boot:run
$ Run endpoint Rest API:
A) Employee
1. Get list Employee:
    curl --location --request GET 'http://localhost:8080/employee'
2. Find by employeeId
 curl --location --request GET 'http://localhost:8080/employee/{id}'
3. Create new Employee
    curl --location --request POST 'http://localhost:8080/employee' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "fistName": "Dung",
        "lastName": "Bui Tien",
        "phoneNumber": "0773245512",
        "address": "Can Tho",
        "department": "{\"name\": '\''BA'\'',\"description\": '\'''\''}"
    }'
4. Update with PUT metho
    curl --location --request PUT 'http://localhost:8080/employee/2' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "fistName": "Minh",
        "lastName": "Nguyen Le",
        "phoneNumber": "",
        "address": "Bac Ninh ",
        "department": "{\"name\": '\''Develop'\'',\"description\": '\'''\''}"
    }'
5/ Delete
    curl --location --request DELETE 'http://localhost:8080/employee/{id}' \
    --data-raw ''
B) Department
```